(function($) {
    "use strict";
    $(window).on('load', function() {
        $('#loader-wrapper').delay(100).fadeOut(500)
    });
    $(window).on('scroll', function() {
        if ($(window).scrollTop() >= 5) {
            $('.header-area').addClass('fixed');
            $('.header-area a.btn').addClass('button-scheme')
        } else {
            $('.header-area').removeClass('fixed');
            $('.header-area a.btn').removeClass('button-scheme')
        }
    });
    $('.water-effect').ripples({
        resolution: 512,
        perturbance: 0.04,
        dropRadius: 20
    });
    $(document).on('scroll', onScroll);
    $('.scroll').on('click', function(e) {
        var scroll_speed = 1000;
        e.preventDefault();
        $(document).off('scroll');
        $('a').each(function() {
            $(this).removeClass('active')
        });
        $(this).addClass('active');
        var target = this.hash,
            menu = target;
        var $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top + 2
        }, scroll_speed, 'swing', function() {
            $(document).on('scroll', onScroll)
        })
    });

    function onScroll(event) {
        var scrollPos = $(document).scrollTop();
        $('#menu_scroll li a').each(function() {
            var currLink = $(this);
            var refElement = $(currLink.attr('href'));
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $('#menu_scroll li a').removeClass('active');
                currLink.addClass('active')
            } else {
                currLink.removeClass('active')
            }
        })
    }
    // $(window).on('load', function() {
    //     var filterValue;
    //     var $grid = $('.works-container');
    //     $grid.isotope({
    //         itemSelector: '.works-item',
    //         layoutMode: 'fitRows',
    //         filter: function() {
    //             var $this = $(this);
    //             return filterValue ? $this.is(filterValue) : true
    //         }
    //     });
    //     $('.works-filter-wrap').on('click', '.works-filter li', function() {
    //         filterValue = $(this).attr('data-filter');
    //         $grid.isotope()
    //     });
    //     $('.works-filter li').on('click', function() {
    //         $('.works-filter li').removeClass('tab-active');
    //         $(this).addClass('tab-active')
    //     })
    // });
    $(document).ready(function() {
        tab();
    });
    function tab() {
        $('.works-container').hide();
        $('.works-container#company').show();
        $('.works-filter li a:first').addClass('tab-active');
        $('.works-filter li a').click(function(){
            var  id_content = $(this).attr("href");
            $('.works-container').hide();
            $(id_content).fadeIn();
            $('.works-filter li a').removeClass('tab-active');
            $(this).addClass('tab-active');
            return false;
        });

    }
    $('.testimonial-carousel').owlCarousel({
        loop: true,
        margin: 15,
        responsiveClass: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: true
            },
            1000: {
                items: 1,
                nav: true
            }
        }
    });
    $('.blog-carousel').owlCarousel({
        loop: true,
        margin: 15,
        responsiveClass: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        dots: false,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
    $(function() {
        $(".counter").countimator()
    });
    var parallax = document.querySelectorAll(".parallax_img"),
        speed = 0.08;
    window.onscroll = function() {
        [].slice.call(parallax).forEach(function(el, i) {
            var rect = el.getBoundingClientRect();
            var windowYOffset = window.pageYOffset,
                elBackgrounPos = "0 " + (windowYOffset * speed) + "px";
            el.style.backgroundPosition = elBackgrounPos
        })
    };
    var EnableDisableForm = function(objectType, btn1, btn1Text) {
        if (objectType == 'Disable') {
            $('#' + btn1).attr('disabled', 'disabled')
        } else {
            $('#' + btn1).removeAttr('disabled')
        }
        $('#' + btn1).val(btn1Text)
    };
    new WOW().init()
})(jQuery);